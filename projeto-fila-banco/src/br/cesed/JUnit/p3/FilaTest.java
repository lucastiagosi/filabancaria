package br.cesed.JUnit.p3;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.itau.estruturaDados.FilaBancaria;

class FilaTest {
	FilaBancaria fila;
	
	@BeforeEach
	void setUp() throws Exception {
		fila = new FilaBancaria();
		
		fila.enqueue("velho", true);

		fila.enqueue("comum", false);

		fila.enqueue("gravida", true);

		fila.enqueue("comum", false);
		
		fila.enqueue("cadeirante", true);
		
		fila.enqueue("comum", false);
	}

	@AfterEach
	void tearDown() throws Exception {
		fila = null;
	}

	@Test
	void enqueueTest1() {
		Assert.assertEquals("velho", fila.enqueue("velho", true ));
	}
	
	@Test
	void dequeueTest() {
		Assert.assertEquals("velho", fila.dequeue());
	}
	
	@Test
	void tamanhoTest() {
		Assert.assertEquals(6, fila.tamanho());
	}
	@Test
	void isEmptyTest() {
		Assert.assertEquals(false, fila.isEmpty());
	}

}

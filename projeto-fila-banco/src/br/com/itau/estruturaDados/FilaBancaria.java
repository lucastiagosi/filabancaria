package br.com.itau.estruturaDados;

public class FilaBancaria {
	private static final int DELIMITADOR = 2;
	private MinhaFila filaNaoPreferencial = new MinhaFila();
	private MinhaFila filaPreferencial = new MinhaFila();

	public void enqueue(Object objeto, boolean ehPreferencial) {

		if (ehPreferencial) {
			filaPreferencial.enqueue(objeto);
		} else {
			filaNaoPreferencial.enqueue(objeto);
		}
	}

	public Object dequeue() {

		Object o = null;

		for (int i = 0; i <= DELIMITADOR; i++) {
			if (i < DELIMITADOR) {

				o = filaNaoPreferencial.dequeue();
			} else {
				o = filaPreferencial.dequeue();
			}
		}

		return o;

	}

	public boolean isEmpty() {
		return filaNaoPreferencial.isEmpty() && filaPreferencial.isEmpty();
	}

	public int tamanho() {
		return filaNaoPreferencial.tamanho() + filaPreferencial.tamanho();
	}

}

package br.com.itau.estruturaDados;

public class MinhaFila {

	private static final int ALOCACAO = 3;
	private static final int UM = 1;

	
	private Object[] arrayInterno = new Object[ALOCACAO];

	private int inseridos;
	
	public void enqueue(Object objeto) {
		if(inseridos >= arrayInterno.length) 
		{
			Object[] arraytemp = new Object[ALOCACAO * arrayInterno.length];
			for (int i = 0; i < arrayInterno.length; i++) {
				arraytemp[i] = arrayInterno[i];
			}
			arrayInterno = arraytemp;
		}
		
		arrayInterno[inseridos] = objeto;
		inseridos ++;
	}
	
	public Object dequeue() {
		 Object front = arrayInterno[0];
		for (int i = 0; i < arrayInterno.length - UM; i++) {
			arrayInterno[i] = arrayInterno[i + UM] ;
		}
		return front;
	}
	
	public boolean isEmpty() {
		return inseridos == 0;
	}
	
	public int tamanho() {
		return inseridos;
	}
}